package com.franklyn.app.carddetailfinder.card_finder

import android.os.Bundle
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.franklyn.app.carddetailfinder.R
import com.franklyn.app.carddetailfinder.card_finder.card_details.CardDetailsFragment
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class CardDetailsFragmentTest {
    private var fragmentScenario: FragmentScenario<CardDetailsFragment> ?= null

    @Before
    fun setUpFragment() {
        //Fragment scenario
        val bundle = Bundle()
        bundle.putBoolean("isTestMode", true)
        //set up fragment with testing mode
        fragmentScenario = launchFragmentInContainer<CardDetailsFragment>(bundle,
            R.style.AppTheme, null)
    }

    @Test
    fun testInputButtonClickedAndDetailsCanBeView() {

        val cardInputs = "559432408440"


        //verify card input accepts string value
        onView(withId(R.id.card_input)).perform(typeText(cardInputs))

        //verify Get Details button is clicked
        onView(withId(R.id.get_detail_btn)).perform(click())

        //verify Get Details button is visible and api call is made via progressBar is visible
        onView(withId(R.id.loading_bar)).check(matches(isEnabled()))


        //confirm result was got from local db
        onView(withId(R.id.card_number)).check(matches(withText(cardInputs)))


    }

    @After
    fun clearFragment() {
        fragmentScenario = null
    }
}