package com.franklyn.app.carddetailfinder.card_finder.local_atms

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AtmDAO {

    //Using Coroutines support to insert
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertATM(atmData: AtmData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateNote(atmData: AtmData)

    //Get all ATM details saved
    @Query("SELECT * FROM atms_table ORDER BY atmNumberFromRoom ASC")
    fun getAllATM(): LiveData<List<AtmData>>
    //Get single details
    @Query("SELECT * FROM atms_table WHERE atmNumberFromRoom LIKE :atmNumberFromRoom")
    suspend fun getATMDetails(atmNumberFromRoom: Long): AtmData?

    //Delete all
    @Query("DELETE FROM atms_table")
    suspend fun deleteAllNotes()
    //Delete single note
    @Delete
    suspend fun deleteNote(atmData: AtmData)
}