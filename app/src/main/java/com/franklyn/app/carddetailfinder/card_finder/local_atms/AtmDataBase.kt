package com.franklyn.app.carddetailfinder.card_finder.local_atms

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteOpenHelper

@Database(entities = [AtmData::class], version = 1, exportSchema = false)
abstract class AtmDataBase: RoomDatabase() {
    //access data via
    abstract fun atmDao(): AtmDAO

    companion object {
        @Volatile
        private var instance: AtmDataBase ?= null
        private const val DATABASE_NAME ="atms_table"

        fun getDataBaseInstance(context: Context) = instance ?: synchronized(this) {
            instance ?: Room.databaseBuilder(context.applicationContext,
                AtmDataBase::class.java, DATABASE_NAME).build().also { instance = it }
        }
    }
}