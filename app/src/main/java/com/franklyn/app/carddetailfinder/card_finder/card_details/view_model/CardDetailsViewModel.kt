package com.franklyn.app.carddetailfinder.card_finder.card_details.view_model

import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.franklyn.app.carddetailfinder.card_finder.card_details.component.FragmentVariables
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response.AtmDetails
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.respository.CardDetailsRepo
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmDAO
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmData
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmDataBase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception

class CardDetailsViewModel (private val cardDetailsRepo: CardDetailsRepo)
    : ViewModel() {

    private val _logTAG = CardDetailsViewModel::class.java.simpleName

    private var appIsUnderTest: Boolean = false

    private var contextVariables: FragmentVariables ?= null
    private lateinit var atmDAO: AtmDAO

    private var _progressBar = MutableLiveData<Boolean>()
    val progressBar : LiveData<Boolean> = _progressBar
    private var _snackBarError = MutableLiveData<String>()
    val snackBarError : LiveData<String> = _snackBarError
    private var _hasCamera = MutableLiveData<Boolean>()
    val hasCamera : LiveData<Boolean> = _hasCamera
    private var _showScanner = MutableLiveData<Boolean>()
    val showScanner : LiveData<Boolean> = _showScanner
    private var _hasAcceptedPermission = MutableLiveData<String>()
    val hasAcceptedPermission : LiveData<String> = _hasAcceptedPermission


    //inputs and outputs
    private var _inputsObserver: InputsObserver
    val atmNumberFromRoom = MutableLiveData<String>()
    val bankName = MutableLiveData<String>()
    val countryName = MutableLiveData<String>()
    val phoneNumber = MutableLiveData<String>()
    val cardType = MutableLiveData<String>()
    val brandName = MutableLiveData<String>()
    val currencyName = MutableLiveData<String>()
    val cityName = MutableLiveData<String>()
    val urlName = MutableLiveData<String>()
    val prepaidName = MutableLiveData<String>()

    //using Coroutine Scope for Asynchronous call
    //since such call my affect UI, use Job to relate to Asynchronous and Dispatcher for UI scope
    private val _viewModelJob = Job()
    private val _scope = CoroutineScope(_viewModelJob + Dispatchers.Main)

    init {
        _inputsObserver = InputsObserver.getInputsObservable
        _progressBar.value = false
        _snackBarError.value =""
        _hasCamera.value = false
        _showScanner.value = false
        _hasAcceptedPermission.value = ""

        clearOldDetailsValue()
    }

    //Input details from observer
    val inputEdit = _inputsObserver.inputEditText

    //server error
    private val _getServerError = cardDetailsRepo.getErrorResponse
    val getServerError: LiveData<String> = _getServerError
    //server response
    val serverResponse = cardDetailsRepo.atmDetails

    fun setContextDagger(contextVariables: FragmentVariables?) {
        this.contextVariables =contextVariables
        //set up database Dao with context
        if (contextVariables != null) {
            atmDAO = AtmDataBase.getDataBaseInstance(contextVariables.context).atmDao()
        }
    }

    fun onGetDetailsHandler() {
        //check if inputEdit is not empty then, call detail from repository
        val atmNumber = inputEdit.value!!
        when {
            atmNumber.isEmpty() -> //show error empty input
                toggleProgressBar(false, "Invalid ATM card number")
            atmNumber.length in 1..7 -> //show error for atm number too small for api check
                toggleProgressBar(false, "ATM card number must be at least 8 digit")
            else -> {
                //hide button and show loader
                toggleProgressBar(true)
                //call api to get details using Coroutine
                _scope.launch {
                    cardDetailsRepo.getAtmDetails(atmNumber.trim(), atmDAO, appIsUnderTest)
                }

            }
        }
    }

    //When scanner is done and ATM number is valid
    fun onScanFinishedWithATMNumber(atmString: String) {
        inputEdit.value = atmString
        onGetDetailsHandler()
    }

    //When scan button is clicked show, when result is from api hide
    fun onScannerHandler(scan: String ="") {
        //Check if user accepted permission
        _hasAcceptedPermission.value = scan
    }

    fun startScannerView(scan: String ="") {
        _showScanner.value = scan  == "true"
    }

    val scannerIsShowing =  { _showScanner.value!! && _hasAcceptedPermission.value == "true"}

    fun viewSavedCardDetails(atmData: AtmData?) {
        atmData?.let { displayATMDetails(it) }
    }

    fun <T>displayATMDetails(atmDetails: T) {
        onScannerHandler()
        try {
            //clear old before set new data
            clearOldDetailsValue()
            //set ATM details view content
            if (atmDetails is AtmDetails) {
                atmNumberFromRoom.value = inputEdit.value
                if (atmDetails.bank != null) {
                    val bankObj = atmDetails.bank
                    bankObj.name?.let {
                        val bank = if (it.toLowerCase().contains("bank")) it
                        else "$it Bank"
                        bankName.value = bank
                    }
                    bankObj.city?.let {
                        cityName.value = if(it.isEmpty()) "" else it
                    }

                    bankObj.url?.let {
                        urlName.value = if(it.isEmpty()) "" else it
                    }
                }
                atmDetails.country?.let { country ->
                    country.emoji?.let {
                        val emoji = if(it.isNotEmpty()) it
                        else atmDetails.country.alpha2
                        countryName.value = "${atmDetails.country.name} ($emoji)"
                    }
                }
                atmDetails.bank?.let { bank ->
                    bank.phone?.let {
                        phoneNumber.value = it
                    }
                }
                atmDetails.scheme?.let {
                    cardType.value = it
                }
                if (atmDetails.type != null && atmDetails.brand != null) {
                    brandName.value = "${atmDetails.type} ${atmDetails.brand} card"
                }
                if (atmDetails.country != null) {
                    atmDetails.country.currency?.let {
                        currencyName.value = it
                    }
                }
                if (atmDetails.prepaid != null) {
                    prepaidName.value = "${atmDetails.prepaid}"
                }
            } else if (atmDetails is AtmData) {
                inputEdit.value = atmDetails.atmNumberFromRoom.toString()
                atmNumberFromRoom.value = atmDetails.atmNumberFromRoom.toString()
                bankName.value = atmDetails.bankName
                countryName.value = atmDetails.countryName
                phoneNumber.value = atmDetails.phoneNumber
                cardType.value = atmDetails.cardType
                brandName.value = atmDetails.brandName
                currencyName.value = atmDetails.currencyName
                cityName.value = atmDetails.cityName
                urlName.value = atmDetails.urlName
                prepaidName.value = atmDetails.prepaidName
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    //When save atm result icon is clicked
    fun saveAtm() {
        if (!inputEdit.value.isNullOrEmpty() && !atmNumberFromRoom.value.isNullOrEmpty()) {
            _scope.launch {
                val inputNumber = inputEdit.value!!.trim().toLong()
                //check is inputNumber is not already saved
                var message = if (atmDAO.getATMDetails(inputNumber)?.atmNumberFromRoom != null
                    && atmDAO.getATMDetails(inputNumber)!!.atmNumberFromRoom  > 0 ) {
                    "$inputNumber Re-saved"
                } else {
                    "$inputNumber Saved"
                }
                atmDAO.insertATM(AtmData(inputEdit.value!!.trim().toLong(),
                    bankName.value!!, countryName.value!!, phoneNumber.value!!,
                    cardType.value!!, brandName.value!!, currencyName.value!!,
                    cityName.value!!, urlName.value!!, prepaidName.value!!))
                toggleProgressBar(false, message)
            }
        } else{
            //show error, no data to save
            toggleProgressBar(false, "No ATM Card details to save.")
        }
    }

    //Divide number in 4 places. This is replicate the structure of an ATM card
    //be inserted in a payment system
    fun onInputTextChange(sChar: CharSequence, start: Int, before: Int, count: Int) {
        _inputsObserver.onInputTextChange(sChar, start, before, count)
    }


    fun toggleProgressBar(value: Boolean, errorString: String =""){
        _progressBar.value =value
        _snackBarError.value =errorString
    }

    fun checkIfUserHasCamera(context: Context?) {
        if (context != null) {
            _hasCamera.value = context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
        }
    }


    private fun clearOldDetailsValue() {
        atmNumberFromRoom.value = ""
        bankName.value = ""
        countryName.value = ""
        phoneNumber.value = ""
        cardType.value = ""
        brandName.value = ""
        currencyName.value = ""
        cityName.value = ""
        urlName.value = ""
        prepaidName.value = ""
    }

    fun showUpCameraSourceError(error: String ="") {
        var errorView = if(error.isEmpty()) {
            "Scanner could be set up, please retry or use the input box above"
        } else error
        _getServerError.value = errorView
    }

    fun setErrorStringEmpty(){
        _getServerError.value = ""
        _snackBarError.value =""
        toggleProgressBar(false)
    }

    fun appIsUnderTestMode(isTest: Boolean) {
        appIsUnderTest = isTest
    }


    override fun onCleared() {
        super.onCleared()
        _viewModelJob.cancel()
    }
}