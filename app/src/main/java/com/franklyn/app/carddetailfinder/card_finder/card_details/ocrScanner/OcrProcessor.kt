package com.franklyn.app.carddetailfinder.card_finder.card_details.ocrScanner

import android.util.Log
import android.util.SparseArray
import com.franklyn.app.carddetailfinder.card_finder.card_details.camera.GraphicOverlay
import com.franklyn.app.carddetailfinder.card_finder.card_details.view_model.CardDetailsViewModel
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import kotlinx.coroutines.*





class OcrProcessor (private val cardDetailsViewModel: CardDetailsViewModel,
                    private val graphicOverlay: GraphicOverlay<OcrGraphic>): Detector.Processor<TextBlock> {

    private val _logTAG = OcrProcessor::class.java.simpleName

    private val _processorJob = Job()
    private var _scope = CoroutineScope(_processorJob + Dispatchers.Main)
    private var scan = true



    override fun release() {
        graphicOverlay.clear()
        _processorJob.cancel()
        Log.i(_logTAG, "_camera stop job")
    }

    //Detector may block the UI since this will be on a receiveDetections callback
    //to be safe using a parallel method to get the string data
     override fun receiveDetections(detector: Detector.Detections<TextBlock>?) {
        graphicOverlay.clear()
        val items: SparseArray<TextBlock> = detector!!.detectedItems
        if (items.size() != 0) {
            //get input text on a background tread using Coroutine
            _scope.launch {
                val atmString =getItemBlock(items)
                //Only atmString is not empty then, there was a successful scan
                //Set input field with string and make api call to get
                //card details
                if (atmString.isNotEmpty())
                    cardDetailsViewModel.onScanFinishedWithATMNumber(atmString)
            }
        }
    }

    private suspend fun getItemBlock(items: SparseArray<TextBlock>): String {
        var value =""
        return withContext(Dispatchers.Main) {
            for (i in 0 until items.size()) {

                //Constraint for ATM card number, must be digit and greater than or equal to 16
                //But base on user experience, let scan from 5 upward(Not including Expiring data)
                //and allow just atm number in view model
                val textBlock = items.valueAt(i)
                if (textBlock != null
                    && textBlock.value != null
                    && textBlock.value.length >= 6) {
                    Log.i(_logTAG,"TextBlock seen: ${textBlock.value}")
                    //check if textBlock is just digit and it is an atm number
                    if (textBlock.value.length >= 15) {
                        val scannedValue = textBlock.value.replace(" ","")

                        if (scannedValue.matches("\\d+".toRegex())) {
                            //get only numbers, from the ATM card
                            Log.i(_logTAG,"TextBlock ATM: ${textBlock.value}")
                            value = scannedValue
                        }
                    }
                    //show scanned of length 5 and above
                    val graphic = OcrGraphic(graphicOverlay, textBlock)
                    graphicOverlay.add(graphic)

                } else {
                    //skip current value scan to next
                    continue
                }
            }
            return@withContext value
        }
    }

}