package com.franklyn.app.carddetailfinder.card_finder.card_details

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.franklyn.app.carddetailfinder.R
import com.franklyn.app.carddetailfinder.card_finder.card_details.camera.CameraSourcePreview
import com.franklyn.app.carddetailfinder.card_finder.card_details.camera.GraphicOverlay
import com.franklyn.app.carddetailfinder.card_finder.card_details.component.ContextModule
import com.franklyn.app.carddetailfinder.card_finder.card_details.component.DaggerClassComponent
import com.franklyn.app.carddetailfinder.card_finder.card_details.component.FragmentVariables
import com.franklyn.app.carddetailfinder.card_finder.card_details.ocrScanner.OcrGraphic
import com.franklyn.app.carddetailfinder.card_finder.card_details.ocrScanner.OcrSource
import com.franklyn.app.carddetailfinder.card_finder.card_details.view_model.CardDetailsInject
import com.franklyn.app.carddetailfinder.card_finder.card_details.view_model.CardDetailsViewModel
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmData
import com.franklyn.app.carddetailfinder.databinding.FragmentCardDetailsBinding
import com.google.android.gms.vision.CameraSource
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_card_details.*
import java.io.IOException
import java.lang.Exception
import java.lang.RuntimeException
import javax.inject.Inject

class CardDetailsFragment : Fragment(){

    private val _logTAG = CardDetailsFragment::class.java.simpleName

    private lateinit var _cardViewModel : CardDetailsViewModel
    private var _cameraSource: CameraSource?= null
    private var ocrSource: OcrSource?= null
    private var preview: CameraSourcePreview? = null
    private var graphicOverlay: GraphicOverlay<OcrGraphic> ?= null

    private val camera = Manifest.permission.CAMERA
    private val write = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private val read = Manifest.permission.READ_EXTERNAL_STORAGE
    private val permissionCode = 3711

    @Inject//injection cannot be private
    lateinit var contextVariables: FragmentVariables


    private var appIsUnderTest: Boolean = false
    private val isTesting = "isTestMode"


    private var cardDetailsInterface: CardDetailsInterface ?= null
    interface CardDetailsInterface {
        fun savedCardDetails(): AtmData?
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CardDetailsInterface) {
            cardDetailsInterface = context
        }
        else {
            try {
                throw RuntimeException("$context implement CardDetailsInterface in CardActivity")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        cardDetailsInterface = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        //set up view model
        val factory = CardDetailsInject.createSingleFactor()
        _cardViewModel = ViewModelProvider(this, factory)
            .get(CardDetailsViewModel::class.java)

        //create dagger dependency and inject fragment class context
        //to be used in view model
        val component = DaggerClassComponent.builder()
            .contextModule(ContextModule(requireActivity())).build()
        component.injectCardDetailFragment(this)
        //set test mode if app is under unit test
        _cardViewModel.setContextDagger(contextVariables)

        // Inflate the layout for this fragment using data binding
        val cardBinding : FragmentCardDetailsBinding = DataBindingUtil
            .inflate(inflater, R.layout.fragment_card_details, container, false)
        //set up life cycle with data binding
        cardBinding.cardViewModel = _cardViewModel
        cardBinding.lifecycleOwner = this

        return cardBinding.root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //set fragment has menu overflow
        setHasOptionsMenu(true)

        //set up test mode from bundle
        val bundle = arguments
        if (bundle != null && bundle.containsKey(isTesting)) {
            appIsUnderTest = bundle.getBoolean(isTesting, false)
            _cardViewModel.appIsUnderTestMode(appIsUnderTest)

        }

        preview = preview_scan as? CameraSourcePreview
        graphicOverlay = graphic_overlay as? GraphicOverlay<OcrGraphic>

        //check if user has camera feature
        _cardViewModel.checkIfUserHasCamera(activity)
        _cardViewModel.hasCamera.observe(requireActivity(), Observer {
            //if user has camera, ask for camera and storage permission
            if(it) {
                checkCameraStoragePermission()
            }
            Log.i(_logTAG, "_cameraSource hasCamera is $it")

        })


        //snackBar action from view model
        _cardViewModel.snackBarError.observe(requireActivity(), Observer { error ->
            //if error is not empty, show snackBar error
            if (error.isNotEmpty()) {
                //clear any error value so view does not show such again after onResume
                // since live data is being used
                _cardViewModel.setErrorStringEmpty()
                //show snackBar
                showErrorBar(error)
            }
        })

        _cardViewModel.hasAcceptedPermission.observe(requireActivity(), Observer { scan ->
            if (scan  == "true") {
                //check if user accepted all permissions
                if (returnPermissionState()) {
                    _cardViewModel.startScannerView(scan)
                } else {
                    getCameraStoragePermission()
                }
            } else {
                _cardViewModel.startScannerView("")
            }
        })
        _cardViewModel.showScanner.observe(requireActivity(), Observer {
            //if show scanner is true, ie button scan was clicked
            if (it) {
                if (returnPermissionState()) {
                    //set up OcrSource
                    setUpOCR()
                } else {
                    getCameraStoragePermission()
                }
            } else {
                //when scanner is done
                stopScan()
            }
        })

        //observe progress bar
        _cardViewModel.progressBar.observe(requireActivity(), Observer {
            //if it is showing, stop scan and release
            if(it) {
                stopScan()
            }
        })

        //server error message observer
        _cardViewModel.getServerError.observe(requireActivity(), Observer { error ->
            if (error.isNotEmpty()) {
                _cardViewModel.toggleProgressBar(false, error)
                _cardViewModel.onScannerHandler()
            }
        })

        //server response with ATM details
        _cardViewModel.serverResponse.observe(requireActivity(), Observer { response ->
            if (response != null) {
                _cardViewModel.toggleProgressBar(false)
                //remove scanner view
                _cardViewModel.displayATMDetails(response)
            }
        })

        //Check if saved card details was clicked for review
        if (cardDetailsInterface != null) {
            _cardViewModel.viewSavedCardDetails(cardDetailsInterface!!.savedCardDetails())
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.save_menu, menu)
        //super.onCreateOptionsMenu(menu, inflater)
    }

    //Save atm when save is clicked
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.save_atm -> {
                _cardViewModel.saveAtm()
                true
            }
            R.id.view_saved -> {
                requireView().findNavController()
                    .navigate(R.id.action_cardDetailsFragment_to_cardStoredFragment)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onPause() {
        super.onPause()
        stopScan()
    }


    private fun showErrorBar(error: String){

        if(null !=activity){
            val snackBar: Snackbar = Snackbar.make(details_layout, error, Snackbar.LENGTH_LONG)
                .setActionTextColor(
                    ContextCompat
                        .getColor(requireActivity(), android.R.color.white))
            snackBar.view.setBackgroundResource(R.color.colorAccent)
            snackBar.show()
        }
    }

    private fun returnPermissionState(): Boolean {

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            (ContextCompat.checkSelfPermission(requireActivity(), camera)
                    + ContextCompat.checkSelfPermission(requireActivity(), write)
                    + ContextCompat.checkSelfPermission(requireActivity(), read)) == PackageManager
                .PERMISSION_GRANTED
         } else {
             true
         }

    }
    private fun checkCameraStoragePermission() {
        //If view is on display
        if (activity != null) {
            //Show dialog with message why permission is needed
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!returnPermissionState()) {
                    showDialogMessage()
                }
            }
        }
    }

    //setup _cameraSource
    private fun setUpOCR() {
        if (ocrSource == null) {
            ocrSource = OcrSource(_cardViewModel, graphicOverlay!!)
        }
        if (_cameraSource == null){
            _cameraSource = ocrSource!!.createCameraSource(requireActivity())
        }
        //start camera
        startCameraSource()
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    @Throws(SecurityException::class)
    private fun startCameraSource() {

        if (_cameraSource != null) {
            try {
                preview!!.start(_cameraSource!!, graphicOverlay!!)
                Log.i(_logTAG, "Start preview")
            } catch (e: IOException) {
                _cameraSource!!.release()
                _cameraSource = null
                e.printStackTrace()
            }
        } else {
            //If for any reason, camera could not be set up, show error to user
            //This case may never occur but to handle all phone type base on
            //memory allocation
            _cardViewModel.showUpCameraSourceError()
        }
    }


    private fun stopScan(){
        if (preview != null) {
            preview!!.stop()
        }
    }

    private fun releaseScan() {
        if (preview != null) {
            preview!!.stop()
            preview!!.release()
        }
    }

    //This will show a message why permission is needed
    private fun showDialogMessage() {
        //if dialog dismisses show permission dialog
        val builder = AlertDialog.Builder(requireActivity())
            .setTitle("${getString(R.string.app_name)} Request")
            .setMessage(getString(R.string.message))
            .setPositiveButton(R.string.alert_ok){dialog, _ ->
                getCameraStoragePermission()
                dialog.dismiss()
            }

        val dialogView = builder.create()
        dialogView.setCancelable(false)
        dialogView.show()

    }

    private fun getCameraStoragePermission() {
        ActivityCompat.requestPermissions(requireActivity(), arrayOf(camera, write, read), permissionCode)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionCode) {
            if(grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                Log.i(_logTAG, "All Permissions accepted")
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        releaseScan()
    }
}