package com.franklyn.app.carddetailfinder.card_finder.card_details.component

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule (private val context: Context) {

    @Provides
    fun provideContext(): Context {
        return context
    }
}