package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.networks.rest_api

import android.util.Log
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response.AtmDetails
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

const val BASE_URL = "https://lookup.binlist.net/"

interface AtmService {

    @GET("/{atmNumber}")
    fun getATMData(@Path("atmNumber")atmNumber: String):Deferred<Response<AtmDetails>>

    companion object{
        operator fun invoke(): AtmService {
            val requestInterceptor =Interceptor{chain ->

                Log.i("ATM Service", "access url: ${chain.request().url()}")
                return@Interceptor chain.proceed(chain.request())

            }

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(MoshiConverterFactory.create().asLenient())
                .build()
                .create(AtmService::class.java)
        }
    }
}