package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response


import com.squareup.moshi.Json

data class Number(
    @Json(name = "length")
    val length: Int,
    @Json(name = "luhn")
    val luhn: Boolean
)