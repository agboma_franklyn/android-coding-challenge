package com.franklyn.app.carddetailfinder.card_finder.card_details.model.respository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.CardDetailsData
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response.AtmDetails
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmDAO
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmData


class CardDetailsRepo private constructor(private val cardDetailsData: CardDetailsData) {

    private val _logTAG = CardDetailsRepo::class.java.simpleName
    private var _atmDetails = MutableLiveData<Any>()
    val atmDetails: LiveData<Any> = _atmDetails
    val getErrorResponse = cardDetailsData.getErrorResponse

    init {
        _atmDetails.value = null
    }

    /**
     * This method should return user ATM details.
     * This could be from ROOM or binlist api call
     */
    suspend fun getAtmDetails(atmNumberString: String, atmDAO: AtmDAO, isTest: Boolean = false) {
        //check db is detail is not found using atmNumberString as the id.
        //If not found. call api
        Log.i(_logTAG, "isTest mode: $isTest")
        if(atmDAO.getATMDetails(atmNumberString.toLong()) != null
            && atmDAO.getATMDetails(atmNumberString.toLong())!!.atmNumberFromRoom >0) {
            _atmDetails.value = atmDAO.getATMDetails(atmNumberString.toLong())
        } else {
            //If user is testing app return dummy content simulating for api call suspend
            _atmDetails.value = if (isTest) {
                AtmData(atmNumberString.toLong(), "User bank",
                    "Country", "Mobile number",
                    "Card type", "Brand name", "Currency",
                    "City", "web site", "false")
            } else {
                cardDetailsData.performATMDetailsRequest(atmNumberString)
            }
        }

    }

    companion object {
        @Volatile
        private var instance : CardDetailsRepo ?= null

        fun getCardDetailsInstance(mCardDetailsDate: CardDetailsData) = instance ?: synchronized(this) {
            instance ?: CardDetailsRepo(mCardDetailsDate).also { instance = it }
        }
    }
}