package com.franklyn.app.carddetailfinder.card_finder.card_stored

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.franklyn.app.carddetailfinder.R
import com.franklyn.app.carddetailfinder.card_finder.card_details.component.FragmentVariables
import com.franklyn.app.carddetailfinder.card_finder.card_stored.adapter.CardStoredAdapter
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmData
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmDAO
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmDataBase
import kotlinx.android.synthetic.main.fragment_card_stored.*
import java.lang.Exception
import java.lang.RuntimeException
import javax.inject.Inject

class CardStoredFragment : Fragment(), CardStoredAdapter.OnAtmNumberClicked {


    private val _logTAG = CardStoredFragment::class.java.simpleName

    private lateinit var atmDAO: AtmDAO

    @Inject//injection cannot be private
    lateinit var contextVariables: FragmentVariables

    private var cardStoredAdapter: CardStoredAdapter ?= null

    private var isLand: Boolean = false


    private var cardStoreFragmentInterface: CardStoreFragmentInterface ?= null

    interface CardStoreFragmentInterface {
        fun atmClickedNavigateBack(atmData: AtmData)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CardStoreFragmentInterface) {
            cardStoreFragmentInterface = context
        } else {
            try {
                throw RuntimeException("$context implement CardStoreFragmentInterface in CardActivity")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        //clear to avoid memory link
        cardStoreFragmentInterface = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        //Set up database DAO for card stored
        atmDAO = AtmDataBase.getDataBaseInstance(requireActivity()).atmDao()

        return inflater.inflate(R.layout.fragment_card_stored, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Check if view is land scape
        isLand = resources.getBoolean(R.bool.isLand)

        //set up adapter
        cardStoredAdapter = CardStoredAdapter(ArrayList<AtmData>(), this)
        atm_recycler.setHasFixedSize(true)
        atm_recycler.layoutManager = if (isLand) GridLayoutManager(requireActivity(), 2)
        else LinearLayoutManager(requireActivity())
        atm_recycler.adapter= cardStoredAdapter


        //get all atm cards stored
        atmDAO.getAllATM().observe(requireActivity(), Observer { atmDataList ->
            //only show list when atmDataList is not null
            atmDataList?.let { list ->
                if (cardStoredAdapter != null) {
                    cardStoredAdapter!!.setATMCardList(list)
                }
            }
        })

    }

    override fun atmNumberClicked(atmData: AtmData) {
        //Send details to CardDetailsFragment via view model
        //view CardDetailsFragment
        cardStoreFragmentInterface?.let { it.atmClickedNavigateBack(atmData) }
    }


}