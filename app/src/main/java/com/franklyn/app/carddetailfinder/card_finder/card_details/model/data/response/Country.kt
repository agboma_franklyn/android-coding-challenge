package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response


import com.squareup.moshi.Json

data class Country(
    @Json(name = "alpha2")
    val alpha2: String,
    @Json(name = "currency")
    val currency: String?,
    @Json(name = "emoji")
    val emoji: String?,
    @Json(name = "latitude")
    val latitude: Int?,
    @Json(name = "longitude")
    val longitude: Int?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "numeric")
    val numeric: String?
)