package com.franklyn.app.carddetailfinder.card_finder.card_details.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.respository.CardDetailsRepo

//Using factory to send CardDetailsRepo instance class
class CardDetailsFactory (private val cardDetailsRepo: CardDetailsRepo)
    : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CardDetailsViewModel(cardDetailsRepo) as T
    }
}