package com.franklyn.app.carddetailfinder.card_finder.card_details.ocrScanner


import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import android.util.Log
import com.franklyn.app.carddetailfinder.card_finder.card_details.camera.GraphicOverlay
import com.franklyn.app.carddetailfinder.card_finder.card_details.view_model.CardDetailsViewModel
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.text.TextRecognizer


class OcrSource (private val cardDetailsViewModel: CardDetailsViewModel,
                 private val graphicOverlay: GraphicOverlay<OcrGraphic>){

    private val _logTAG = OcrSource::class.java.simpleName

    @SuppressLint("InlinedApi")
    fun createCameraSource(context: Context, autoFocus: Boolean =true, useFlash: Boolean =false)
            : CameraSource? {

        var cameraSource: CameraSource?= null
        // Create the TextRecognizer
        val textRecognizer = TextRecognizer.Builder(context).build()

        // Check if the TextRecognizer is operational.
        if (!textRecognizer.isOperational) {
            Log.i(_logTAG, "Detector dependencies are not yet available.")

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.

            val memory = context.cacheDir
            if ((memory.usableSpace * 100 / memory.totalSpace) > 10) {
                //If user memory is too low, show error
                //memory allocation
                cardDetailsViewModel.showUpCameraSourceError("Device memory is too low for." +
                        " Cannot use the scanning features")
                Log.i(_logTAG, "Low memory storage")
            }
        } else {

            // Set the TextRecognizer's Processor.
            textRecognizer.setProcessor(OcrProcessor(cardDetailsViewModel, graphicOverlay))

            cameraSource = CameraSource.Builder(context, textRecognizer)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1280, 1024)
                .setRequestedFps(2.0f)
                .setAutoFocusEnabled(true)
                .build()


        }

        Log.i(_logTAG, "Has memory storage _cameraSource: ${cameraSource == null}")
        return cameraSource
    }
}