package com.franklyn.app.carddetailfinder.card_finder.local_atms

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "atms_table")
class AtmData () {

    @PrimaryKey
    var atmNumberFromRoom: Long =0
    @ColumnInfo(name = "bankName")
    var bankName: String =""
    @ColumnInfo(name = "countryName")
    var countryName: String =""
    @ColumnInfo(name = "phoneNumber")
    var phoneNumber: String =""
    @ColumnInfo(name = "cardType")
    var cardType: String =""
    @ColumnInfo(name = "brandName")
    var brandName: String =""
    @ColumnInfo(name = "currencyName")
    var currencyName: String =""
    @ColumnInfo(name = "cityName")
    var cityName: String =""
    @ColumnInfo(name = "urlName")
    var urlName: String =""
    @ColumnInfo(name = "prepaidName")
    var prepaidName: String =""

    //Room constructor
    constructor( atmNumberFromRoom: Long,
                 bankName: String, countryName: String, phoneNumber: String,
                 cardType: String, brandName: String, currencyName: String,
                 cityName: String, urlName: String, prepaidName: String): this(){
        this.atmNumberFromRoom = atmNumberFromRoom
        this.bankName = bankName
        this.countryName = countryName
        this.phoneNumber = phoneNumber
        this.cardType = cardType
        this.brandName = brandName
        this.currencyName = currencyName
        this.cityName = cityName
        this.urlName = urlName
        this.prepaidName = prepaidName
    }

}