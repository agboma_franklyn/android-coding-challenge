package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response


import com.squareup.moshi.Json

data class AtmDetails(
    @Json(name = "bank")
    val bank: Bank?,
    @Json(name = "brand")
    val brand: String?,
    @Json(name = "country")
    val country: Country?,
    @Json(name = "number")
    val number: Number?,
    @Json(name = "prepaid")
    val prepaid: Boolean?,
    @Json(name = "scheme")
    val scheme: String?,
    @Json(name = "type")
    val type: String?
)