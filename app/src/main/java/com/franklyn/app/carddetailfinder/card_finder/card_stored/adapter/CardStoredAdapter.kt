package com.franklyn.app.carddetailfinder.card_finder.card_stored.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.franklyn.app.carddetailfinder.R
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmData
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.fragment_card_details.view.*

class CardStoredAdapter() : RecyclerView.Adapter<CardStoredAdapter.ItemAtmHolder>() {

    private var context: Context?= null
    private var atmData: List<AtmData> ?= null
    private var atmClicked: OnAtmNumberClicked ?= null

    constructor(atmData: List<AtmData>, atmClicked: OnAtmNumberClicked): this() {
        this.atmData = atmData
        this.atmClicked = atmClicked
    }

    interface OnAtmNumberClicked {
        fun atmNumberClicked(atmData: AtmData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAtmHolder {
        this.context = parent.context
        return ItemAtmHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item, parent, false))
    }

    override fun getItemCount(): Int {
        return if (atmData != null) atmData!!.size else 0
    }

    override fun onBindViewHolder(holder: ItemAtmHolder, position: Int) {
        holder.setItemView(atmData!![position])
    }

    fun setATMCardList(atmData: List<AtmData>) {
        this.atmData = atmData
        notifyDataSetChanged()
    }

    inner class ItemAtmHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        //initialize clicks
        init {
            itemView.setOnClickListener(this)
        }

        /**
         * When an atm is clicked
         */
        override fun onClick(v: View?) {
            atmClicked?.let {
                it.atmNumberClicked(atmData!![adapterPosition])
            }
        }

        fun setItemView(atmData: AtmData?) {
            atmData?.let {result ->
                itemView.atm_number.text = "${result.atmNumberFromRoom}"
            }
        }

    }
}