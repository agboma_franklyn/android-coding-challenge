package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response


import com.squareup.moshi.Json

data class Bank(
    @Json(name = "city")
    val city: String?,
    @Json(name = "name")
    val name: String?,
    @Json(name = "phone")
    val phone: String?,
    @Json(name = "url")
    val url: String?
)