package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.networks.service_call

import java.lang.Exception

sealed class Result<out T: Any> {
    data class Success<out T: Any>(val output :T): Result<T>()
    data class Error(val exception: Exception): Result<Nothing>()
}