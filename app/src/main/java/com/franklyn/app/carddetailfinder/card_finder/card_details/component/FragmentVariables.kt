package com.franklyn.app.carddetailfinder.card_finder.card_details.component

import android.content.Context
import android.util.Log
import javax.inject.Inject


//using construction injection
class FragmentVariables @Inject constructor(val context: Context)