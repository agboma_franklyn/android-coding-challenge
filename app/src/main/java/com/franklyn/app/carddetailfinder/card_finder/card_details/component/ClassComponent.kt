package com.franklyn.app.carddetailfinder.card_finder.card_details.component

import com.franklyn.app.carddetailfinder.card_finder.card_details.CardDetailsFragment
import com.franklyn.app.carddetailfinder.card_finder.card_stored.CardStoredFragment
import dagger.Component

@Component (modules = [ContextModule::class])
interface ClassComponent {

    fun injectCardDetailFragment(cardDetailsFragment: CardDetailsFragment)

    fun injectCardStoredFragment(cardStoredFragment: CardStoredFragment)
}