package com.franklyn.app.carddetailfinder.card_finder.card_details.view_model

import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.CardDetailsData
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.networks.rest_api.AtmService
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.respository.CardDetailsRepo

//Using a single instance of CardDetailsFactory
object CardDetailsInject {

    fun createSingleFactor() : CardDetailsFactory {
        val repository = CardDetailsRepo.getCardDetailsInstance(CardDetailsData
            .getCardDetailsInstance(AtmService.invoke()))
        return CardDetailsFactory(repository)
    }
}