package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.networks.service_call

import android.util.Log
import androidx.lifecycle.MutableLiveData
import retrofit2.Response
import java.io.IOException

open class BaseDataSource {

    private val _logTAG =BaseDataSource::class.java.simpleName

    var getErrorMessage =MutableLiveData<String>()
    var errorCode: Int =0

    init {
        getErrorMessage.value =""
    }

    suspend fun <T: Any> safeApiCall(
            call: suspend() -> Response<T>) :T? {

        var data: T?=null
        //set to default
        getErrorMessage.value =""
        //get response result
        when(val result: Result<T> =safeApiResult(call)){
            is Result.Success ->data =result.output
            is Result.Error ->{
                Log.i(_logTAG,"${result.exception}")
            }
        }

        return data
    }

    private suspend fun <T: Any> safeApiResult(call: suspend() -> Response<T>) : Result<T> {
        try {
            val response = call.invoke()
            if(response.isSuccessful) {
                val collectedResponse =response.body()!!
                return Result.Success(collectedResponse)
            }
            //When a api was successfully hit but invalid response
            errorCode =response.code()
            getErrorMessage.value =errorMessage(errorCode)
            //other wise return error
            return Result.Error(IOException("Connection Oops, something went wrong"))
        }
        catch (e: Exception){
            e.printStackTrace()
            //send error code.
//            if(errorCode == 0)
//                errorCode =900
            getErrorMessage.value =errorMessage(errorCode)
            //other wise return error
            return Result.Error(IOException("Connection Oops, something went wrong due " +
                    "to ${e.message ?: ""}"))
        }
    }

    private fun errorMessage(code: Int): String{
        Log.i(_logTAG, "Error code: $code")

        /**
         * From binlist.net,
         * When a card if not found, error code is 404
         * and when request has exceeded 10 for a minute, it returns 429
         */
        var error = when (code) {
            in 400..405 -> "User card details not found."
            in 406..430 -> "You have exceeded the limit, try in 1 hour time"
            else -> "Connection error, please check your internet connection and retry."
        }

        Log.i(_logTAG, "error")

        return error
    }

}