package com.franklyn.app.carddetailfinder.card_finder.card_activity

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavArgument
import androidx.navigation.NavController
import androidx.navigation.NavHost
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.franklyn.app.carddetailfinder.R
import com.franklyn.app.carddetailfinder.card_finder.card_details.CardDetailsFragment
import com.franklyn.app.carddetailfinder.card_finder.card_stored.CardStoredFragment
import com.franklyn.app.carddetailfinder.card_finder.local_atms.AtmData
import kotlinx.android.synthetic.main.activity_card.*

class CardActivity : AppCompatActivity(),
    CardDetailsFragment.CardDetailsInterface,
    CardStoredFragment.CardStoreFragmentInterface {

    private val _logTAG  =CardActivity::class.java.simpleName
    private lateinit var navigationController: NavController
    private var savedAtmData: AtmData ?= null
    private val inputString = "Inputs"
    private var screen =inputString
    private val SCREEN_KEY ="screenKey"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)

        //set navigation controller
        navigationController = Navigation.findNavController(this, R.id.cards_fragments)
        //set action bar with navigation controller
        NavigationUI.setupActionBarWithNavController(this, navigationController)
    }

    //Take navigation stack back to parent when back button is clicked from
    //toolbar
    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navigationController, null)
    }

    //Save current screen name for ease onBackPressed callback
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(SCREEN_KEY, screen)
    }

    //Get saved current screen name
    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if ( savedInstanceState != null) {
            screen = savedInstanceState.getString(SCREEN_KEY, inputString)
        }
    }

    override fun savedCardDetails(): AtmData? {
        return if (savedAtmData != null) {
            //send savedAtmData to CardDetailsFragment and after which, set it to null
             savedAtmData!!.also { savedAtmData = null }
        } else null
    }

    override fun atmClickedNavigateBack(atmData: AtmData) {
        navigationController.navigateUp()
        savedAtmData = atmData
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}