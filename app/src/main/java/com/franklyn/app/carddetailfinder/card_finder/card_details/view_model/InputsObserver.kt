package com.franklyn.app.carddetailfinder.card_finder.card_details.view_model

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData

class InputsObserver : BaseObservable() {

    private val _logTAG = InputsObserver::class.java.simpleName

    companion object {
        @Volatile
        private var instance : InputsObserver ?= null

        val getInputsObservable = instance ?: synchronized(this) {
            instance ?: InputsObserver().also { instance = it }
        }
    }

    @Bindable
    val inputEditText = MutableLiveData<String>()

    init {
        clearAllInputs()
    }

    fun onInputTextChange(sChar: CharSequence, start: Int, before: Int, count: Int) {
        Log.i(_logTAG, "$sChar count: $count")
        /**
         * This is to simulate the ATM card inputs as it is on a payment system
         * Note card number are 16 or 19 but add the double space in-between,
        then it becomes 22 or 27.
        so check when number is increased by 1 in division of 4 then add 4+2 substring 1 where +2 is double space
        and while if 22 or 27. Then, to delete, we check for less than added.
         */
        if (sChar.length == 4 || sChar.length == 10
            || sChar.length == 16 || sChar.length == 22) {
            Log.i(_logTAG, "in")

        }
    }

    private fun clearAllInputs() {
        inputEditText.value = ""
    }
}