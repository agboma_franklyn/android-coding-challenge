package com.franklyn.app.carddetailfinder.card_finder.card_details.model.data

import androidx.lifecycle.MutableLiveData
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.networks.rest_api.AtmService
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.networks.service_call.BaseDataSource
import com.franklyn.app.carddetailfinder.card_finder.card_details.model.data.response.AtmDetails

class CardDetailsData private constructor(private val atmService: AtmService): BaseDataSource() {

    private val logTAG = CardDetailsData::class.java.simpleName
    val getErrorResponse : MutableLiveData<String> = getErrorMessage

    /**
     * This method should return user ATM details.
     * This could be from ROOM or binlist api call
     */
    suspend fun performATMDetailsRequest(atmNumberString: String): AtmDetails? {
        return safeApiCall(call = {atmService.getATMData(atmNumberString).await()})
    }

    companion object {
        @Volatile
        private var instance : CardDetailsData ?= null

        fun getCardDetailsInstance(mAtmService: AtmService) = instance ?: synchronized(this) {
            instance ?: CardDetailsData(mAtmService).also { instance = it }
        }
    }
}